import { Component } from '@angular/core';
import {IVendors} from './vendortype';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'piq';
  billTitle: string = 'Freguesia Cheese';
  showTable = false;
  vendors: IVendors[] = [
    {
      invoiceno: 3526,
      invoicetype: "Receipt",
      restaurant: `Monty's Cheese Shop`,
      subtotal: 1300.00,
      tax: 26.32,
      postingdate: "Jul 14,2017",
      invoicedate: "Jul 09,2017",
      duedate: "Sep 09,2017",
      total: 10000.00
    },
  ];


  toggleTabie(): void {
    this.showTable = !this.showTable;
  }
}
