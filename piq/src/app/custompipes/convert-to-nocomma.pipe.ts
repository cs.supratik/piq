import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'convertToNocomma'
})
export class ConvertToNocommaPipe implements PipeTransform {
    transform(val:number): string {
        if(val !== undefined && val !== null) {
            let charcomma = ',';
            return val.toString().replace(charcomma,"");
        }
        else {
            return "";
        }
    }
}