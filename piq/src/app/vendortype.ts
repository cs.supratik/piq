export interface IVendors {
      invoiceno: number;
      invoicetype: string;
      restaurant: string;
      subtotal: number;
      tax: number;
      postingdate: string;
      invoicedate: string,
      duedate: string;
      total: number;
}